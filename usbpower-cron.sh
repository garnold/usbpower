#!/bin/bash

PROG=/usr/local/bin/usbpower
IFACE_NAME=enp2s4
IFACE_ADDR=192.168.1.10
PING_COUNT=3
INET_HOST=8.8.8.8
LOG=/var/log/usbpower-cron.log

log() {
  echo "$(date +%Y%m%d-%H%M%S): $@" >> "$LOG"
}

die() {
  log "Error: $@"
  exit 1
}

log "running"

# Check that the program is in the path.
command -v "$PROG" > /dev/null 2>&1 || die "$PROG not found"

# If status if off, turn it on and finish.
"$PROG" | grep -q off && { \
  "$PROG" on && log "powered on" && exit 0 || die "power on failed"; }

# If Ethernet address is down, bring the iface up; not checking exit status.
ping -c 1 $IFACE_ADDR > /dev/null || { \
  /etc/init.d/net.$IFACE_NAME start; log "started $IFACE_NAME"; exit 0; }

# If no internet connectivity, power cycle the outlet.
ping -c $PING_COUNT $INET_HOST > /dev/null || { \
  "$PROG" off && sleep 5 && "$PROG" on && log "power cycled" || \
  die "power cycle failed"; }

log "okay"

/*
 * Program for controlling the "USB Net Power 8800" USB-controlled outlet.
 * Copyright (c) 2012 Jeff Epler
 * Adapted by Gilad Arnold
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <err.h>
#include <libgen.h>
#include <stdio.h>
#include <string.h>
#include <usb.h>


#define USBPOWER_VENDOR_ID 0x067b
#define USBPOWER_PRODUCT_ID 0x2303

#define CMD_TOGGLE "toggle"
#define CMD_ON "on"
#define CMD_OFF "off"

#define USAGE_STR \
    "Usage: %s [ " CMD_TOGGLE " | " CMD_ON " | " CMD_OFF " ]\n"

static struct usb_dev_handle *usbpower_dev = NULL;


void
usbpower_init(void)
{
    if (usbpower_dev)
        return;

    usb_init();
    usb_find_busses();
    usb_find_devices();

    struct usb_bus *bus_list = usb_get_busses();
    struct usb_bus *bus;
    struct usb_device *dev;
    for (bus = bus_list; bus; bus = bus->next)
        for (dev = bus->devices; dev; dev = dev->next)
	    if (dev->descriptor.idVendor == USBPOWER_VENDOR_ID &&
		dev->descriptor.idProduct == USBPOWER_PRODUCT_ID) {
                if (!(usbpower_dev = usb_open(dev)))
                    err(1, "usb_open");
                return;
            }
    errx(1, "usbpower device not found");
}

int
usbpower_is_on(void)
{
    usbpower_init();
    unsigned char res;
    int ret = usb_control_msg(usbpower_dev, 0xc0, 1, 0x81, 0, (char *)&res,
			      sizeof(res), 1000);
    if (ret < 0)
        err(1, "usb_control_msg");
    if (ret != 1)
	errx(1, "usb_control_msg: unexpected written data size (%d)", ret);
    return (res == 0xa0);
}

void
usbpower_set(int on)
{
    usbpower_init();
    char code = on ? 0xa0 : 0x20;
    int ret = usb_control_msg(usbpower_dev, 0x40, 1, 0x1, code, NULL, 0, 1000);
    if (ret < 0)
        err(1, "usb_control_msg error (%d)", ret);
}

void
usage(int ret, char *prog_name)
{
    fprintf(stderr, USAGE_STR, basename(prog_name));
    exit(ret);
}

int
main(int argc, char **argv)
{
    if (argc == 1)
	printf("status: %s\n", usbpower_is_on() ? CMD_ON : CMD_OFF);
    else if (argc == 2) {
	if (!strcmp(argv[1], CMD_TOGGLE))
	    usbpower_set(!usbpower_is_on());
	else if (!strcmp(argv[1], CMD_ON))
	    usbpower_set(1);
	else if (!strcmp(argv[1], CMD_OFF))
	    usbpower_set(0);
	else
	    usage(strcmp(argv[1], "-h") ? 1 : 0, argv[0]);
    } else
	usage(1, argv[0]);

    return 0;
}

CFLAGS=-Wall -Werror

.PHONY: all clean

all: usbpower

clean:
	rm -f usbpower *.o

usbpower: -lusb
